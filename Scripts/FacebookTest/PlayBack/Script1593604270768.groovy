import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.facebook.com/')

WebUI.setText(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/password_emailTxtbx'), 
    'samuel')

WebUI.setText(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/firstNameTxtbx'), 
    'Sam')

WebUI.setText(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/lastNameTxtbx'), 
    'Badu')

WebUI.setText(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/phoneNumberTxtbx'), 
    '075889987665')

WebUI.setEncryptedText(findTestObject('Object Repository/SignUpWebElements/Page_Facebook  log in or sign up/input_concat(We couldn  t create your accou_aa95f5'), 
    'fcdLPds+JHgWEir/EYFLTQ==')

WebUI.selectOptionByValue(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/dayDrpdwn'), 
    '20', true)

WebUI.selectOptionByValue(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/monthDrpdwn'), 
    '11', true)

WebUI.selectOptionByValue(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/yearDrpdwn'), 
    '1958', true)

WebUI.click(findTestObject('SignUpWebElements/Page_Facebook  log in or sign up/maleRdbtn'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SignUpWebElements/Page_Facebook  log in or sign up/select_Select your pronounShe Wish her a ha_90e2d2'), 
    '1', true)

WebUI.setText(findTestObject('Object Repository/SignUpWebElements/Page_Facebook  log in or sign up/input_Your pronoun is visible to everyone_c_a78fbb'), 
    'female')

WebUI.closeBrowser()

